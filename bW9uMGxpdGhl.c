/*
* Author  : leCreateur
* Date    : 4/11/2020
* Purpose : Poser les bases de l’interface du mon0lithe.
* Version : 0.1
*/

#include <stdio.h>
#include <stdlib.h>

#include <openssl/rand.h>
#include <openssl/des.h>
#include <openssl/evp.h>

#include <aicompressor.h>
#include <monolithecmd.h>
#include <monolithe.h>

//Librairies utiles 
// aiCompressor de Rémus 
// AES pour sécuriser le projet (peut être OpenSSL)
// mon0litheInterface quand Rémus l’aura finit…

int main(void){
    printf("helloworld\n");
    //1 - Compresser l’Intélligence
    //2 - Séparer les parties compressées.
    //3 - Phase de gel :
    //  a - Avoir le dictionnaire / liste des mots de passes.
    //  x - lier l’IA au monolithe monolithe --Fusion IA_NOM
    //  b - Pour chaque partie de l’IA :
    //      - Chiffrer la partie (mot de passe unique)
    //      - L’ajouter au monolithe
    // Notes :
    //    C’est d’après Rémus le seul moyen de geler l’IA sans la détruire.
    //    C’est fous de se dire que cet ensemble de donnés est vivant et possède des sortes d’organes vitaux.
    //    En fesant avec la technologie de Rémus, nous pourrons "dégivré" progressivement l’IA pour la faire fonctionner, sans totalement la libérer.
    //
    // Phase de dégivrage :
    //1 - Entrer les mots de passe dans l’ordre.
    //  - Ceci va "dégivrer" et automatiquement décompresser les partie de l’IA.
    //  - C’est incompréhensible, vu que l’IA à été compressée puis séparrée...
    // Libérer l’IA (attention détruira le monolithe) :
    //  monolithe -u NOM_IA
    //  Notes : -u pour unleash
    return 0;
}
//TODO : 
// - Implémenter les binaires de rémus.
// - Faire le code.
// - Vérifier si le centre n’a pas un git, mettre ça sur internet ? c’est risqué ?.
